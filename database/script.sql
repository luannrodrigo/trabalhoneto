-- Criação do verba
DROP DATABASE if exists dados182n;
CREATE DATABASE dados182n;
USE dados182n;

-- Criação das tabelas
CREATE TABLE prefeito (
  pre_codigo INT AUTO_INCREMENT PRIMARY KEY,
  pre_nome VARCHAR(50) NOT NULL,
  pre_patrimonio VARCHAR(50) NOT NULL,
  pre_verba VARCHAR(50) NOT NULL
);

CREATE TABLE cidade (
  cid_codigo INT AUTO_INCREMENT PRIMARY KEY,
  cid_nome VARCHAR(30) NOT NULL,
  cid_populacao VARCHAR(15) NOT NULL,
  cid_telefone VARCHAR(20) NOT NULL,
  pre_codigo INT NOT NULL,
  CONSTRAINT `fk_cid_prefeito`
      FOREIGN KEY (pre_codigo) REFERENCES prefeito (pre_codigo)
      ON DELETE CASCADE
      ON UPDATE RESTRICT
);

CREATE TABLE rodovia (
  rod_codigo INT AUTO_INCREMENT PRIMARY KEY,
  rod_nome VARCHAR(50) NOT NULL,
  rod_extensao VARCHAR(50) NOT NULL
);

CREATE TABLE regiao (
  reg_codigo INT AUTO_INCREMENT PRIMARY KEY,
  reg_nome varchar(50) not null,
  reg_producao VARCHAR(10) NOT NULL,
  reg_tipo VARCHAR(15) NOT NULL,
  reg_populacao DECIMAL(12,2) NOT NULL,
  cid_codigo INT NOT NULL,
  rod_codigo INT NOT NULL,

  CONSTRAINT `fk_reg_cidade`
      FOREIGN KEY (cid_codigo) REFERENCES cidade (cid_codigo)
      ON DELETE CASCADE
      ON UPDATE RESTRICT,
  CONSTRAINT `fk_reg_rodovia`
      FOREIGN KEY (rod_codigo) REFERENCES rodovia (rod_codigo)
      ON DELETE CASCADE
      ON UPDATE RESTRICT
);

-- Inserção de dados para testes
INSERT INTO rodovia (rod_nome, rod_extensao)
VALUES
  ('Luis inicio', '1233'),
  ('Chico Buaque', '1233'),
  ('Bil de Blasio', '1233');

INSERT INTO prefeito (pre_nome, pre_patrimonio, pre_verba)
VALUES
  ('Luis inicio', '1233', '12000'),
  ('Chico Buaque', '1233', '12000'),
  ('Bil de Blasio', '1233', '12000');

-- Inserção de dados para testes
INSERT INTO cidade (cid_nome, cid_populacao, cid_telefone, pre_codigo)
VALUES
  ('Stanley Ipkiss', 'stan', '1655554321',1),
  ('Grace Hoper', 'grace', '165551234', 2),
  ('Martin Luther King', 'luther', '165558765', 1 );

INSERT INTO regiao (reg_nome, reg_producao, reg_tipo, reg_populacao, rod_codigo, cid_codigo)
VALUES
  ('ribeirão','Calçado', 'Norte', 00.00, 1, 3),
  ('interior', 'Comercio', 'Sul', 00.00, 2, 1),
  ('aqu','Madeira', 'Oeste', 00.00, 3, 2);

-- Criação das views
CREATE VIEW view_prefeitos AS
SELECT
pre_codigo,
pre_nome,
pre_patrimonio,
pre_verba
FROM prefeito

CREATE VIEW view_rodovias AS
SELECT
rod_codigo,
rod_nome,
rod_extensao
FROM rodovia

CREATE VIEW view_cidades AS
SELECT
cid_codigo,
cid_nome,
cid_populacao,
cid_telefone,
pre_nome
FROM cidade
INNER JOIN prefeito ON cidade.pre_codigo = prefeito.pre_codigo;

CREATE VIEW view_regiaos AS
SELECT
reg_codigo,
reg_nome,
reg_producao,
reg_tipo,
reg_populacao,
rod_nome,
cid_nome
FROM regiao
INNER JOIN cidade ON regiao.cid_codigo = cidade.cid_codigo
INNER JOIN rodovia ON regiao.rod_codigo = rodovia.rod_codigo;


CREATE VIEW view_quantidades AS
SELECT
(SELECT COUNT(cid_codigo) FROM cidade) AS cidades,
(SELECT COUNT(reg_codigo) FROM regiao) AS regiaos;

select * from regiao
