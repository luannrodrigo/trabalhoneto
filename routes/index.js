const express = require('express');
const router = express.Router();

/* GET home page. */
const indexController = require('../controllers/indexController');
router.get('/', indexController.count);

module.exports = router;
