const router = require('express').Router();

const prefeitoController = require('../controllers/prefeitoController');

router.get('/', prefeitoController.list);
router.post('/add', prefeitoController.save);
router.get('/update/:pre_codigo', prefeitoController.edit);
router.post('/update/:pre_codigo', prefeitoController.update);
router.get('/delete/:pre_codigo', prefeitoController.delete);

module.exports = router;
