const router = require('express').Router();

const regiaoController = require('../controllers/regiaoController');

router.get('/', regiaoController.list);
router.post('/add', regiaoController.save);
router.get('/update/:reg_codigo', regiaoController.edit);
router.post('/update/:reg_codigo', regiaoController.update);
router.get('/delete/:reg_codigo', regiaoController.delete);

module.exports = router;
