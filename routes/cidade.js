const router = require('express').Router();

const cidadeController = require('../controllers/cidadeController');

router.get('/', cidadeController.list);
router.post('/add', cidadeController.save);
router.get('/update/:cid_codigo', cidadeController.edit);
router.post('/update/:cid_codigo', cidadeController.update);
router.get('/delete/:cid_codigo', cidadeController.delete);

module.exports = router;
