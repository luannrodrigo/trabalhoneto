const router = require('express').Router();

const rodoviasController = require('../controllers/rodoviaController');

router.get('/', rodoviasController.list);
router.post('/add', rodoviasController.save);
router.get('/update/:rod_codigo', rodoviasController.edit);
router.post('/update/:rod_codigo', rodoviasController.update);
router.get('/delete/:rod_codigo', rodoviasController.delete);

module.exports = router;
