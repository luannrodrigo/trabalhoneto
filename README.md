# Dash City

## Para criar o banco:

` mysql --user=root --password -s < ~/cidades-regiaos-final/database/script.sql`

## Para rodar o projeto:

```
cd cidades-regiaos-final
npm install
node app.js
```
