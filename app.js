const express = require('express'),
      path = require('path'),
      morgan = require('morgan'),
      mysql = require('mysql'),
      myConnection = require('express-myconnection');

const app = express();

// importing routes
//const customerRoutes = require('./routes/customer');
const indexRouter = require('./routes/index');
const regiaosRoutes = require('./routes/regiao');
const cidadeRoutes = require('./routes/cidade');
const prefeitoRoutes = require("./routes/prefeito");
const rodoviaRoutes = require("./routes/rodovia");


// settings
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// middlewares
app.use(morgan('dev'));
app.use(myConnection(mysql, {
  host: 'localhost',
  user: 'root',
  password: 'password',
  port: 3306,
  database: 'dados182n'
}, 'single'));
app.use(express.urlencoded({extended: false}));

// routes
app.use('/', indexRouter);
app.use('/regiaos', regiaosRoutes);
app.use('/cidades', cidadeRoutes);
app.use("/prefeitos", prefeitoRoutes);
app.use("/rodovias", rodoviaRoutes);

// static files
app.use(express.static(path.join(__dirname, 'public')));

// starting the server
app.listen(app.get('port'), () => {
  console.log(`server on port ${app.get('port')}`);
});
