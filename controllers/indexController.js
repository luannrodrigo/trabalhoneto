const controller = {};

controller.count = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('SELECT * FROM view_quantidades', (err, rows) => {
     if (err) {
      res.json(err);
     }
     console.log(rows);
     res.render('index', {
        cidadesCount: rows[0].cidades,
        regiaosCount: rows[0].regiaos,
        prefeitoCount: rows[0].prefeitos,
        rodoviaCount: rows[0].rodovias,
        title: 'Dash Regiões'
     });
    });
  });
};

module.exports = controller;
