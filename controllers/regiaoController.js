const controller = {};

controller.list = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query("SELECT * FROM view_regiaos", (err, regiaos) => {
      if (err) {
        res.json(err);
      }
      res.render("regiaos", {
        data: regiaos
      });
    });
  });
};

controller.save = (req, res) => {
  const data = req.body;
  console.log(req.body);
  req.getConnection((err, connection) => {
    const query = connection.query(
      "INSERT INTO regiao set ?",
      data,
      (err, regiao) => {
        console.log(err);
        res.redirect("/regiaos");
      }
    );
  });
};


controller.edit = (req, res) => {
  const { reg_codigo } = req.params;
  req.getConnection((err, conn) => {
    conn.query(
      "SELECT * FROM regiao WHERE reg_codigo = ?",
      [reg_codigo],
      (err, rows) => {
        console.log(err)
        res.render("editar-regiaos", {
          data: rows[0]
        });
      }
    );
  });
};

controller.update = (req, res) => {
  const { reg_codigo } = req.params;
  const newManager = req.body;
  req.getConnection((err, conn) => {
    conn.query(
      "UPDATE regiao set ? where reg_codigo = ?",
      [newManager, reg_codigo],
      (err, rows) => {
        res.redirect("/regiaos");
      }
    );
  });
};

controller.delete = (req, res) => {
  const { reg_codigo } = req.params;
  req.getConnection((err, connection) => {
    connection.query(
      "DELETE FROM regiao WHERE reg_codigo = ?",
      [reg_codigo],
      (err, rows) => {
        res.redirect("/regiaos");
      }
    );
  });
};

module.exports = controller;
