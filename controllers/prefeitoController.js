const controller = {};

controller.list = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM view_prefeitos', (err, prefeitos) => {
            if (err) {
                res.json(err);
            }
            res.render('prefeitos', {
                data: prefeitos
            });
        });
    });
};

controller.save = (req, res) => {
    const data = req.body;
    console.log(req.body)
    req.getConnection((err, connection) => {
        const query = connection.query('INSERT INTO prefeito set ?', data, (err, prefeito) => {
            console.log(prefeito)
            res.redirect('/prefeitos');
        })
    })
};

controller.edit = (req, res) => {
    const {
        pre_codigo
    } = req.params;
    req.getConnection((err, conn) => {
        conn.query("SELECT * FROM prefeito WHERE pre_codigo = ?", [pre_codigo], (err, rows) => {
            res.render('editar-prefeito', {
                data: rows[0]
            })
        });
    });
};

controller.update = (req, res) => {
    const {
        pre_codigo
    } = req.params;
    const newPrefeito = req.body;
    req.getConnection((err, conn) => {

        conn.query('UPDATE prefeito set ? where pre_codigo = ?', [newPrefeito, pre_codigo], (err, rows) => {
            res.redirect('/prefeitos');
        });
    });
};

controller.delete = (req, res) => {
    const {
        pre_codigo
    } = req.params;
    req.getConnection((err, connection) => {
        connection.query('DELETE FROM prefeito WHERE pre_codigo = ?', [pre_codigo], (err, rows) => {
            res.redirect('/prefeito');
        });
    });
}

module.exports = controller;
