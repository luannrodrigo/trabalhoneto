const controller = {};

controller.list = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('SELECT * FROM view_cidades', (err, cidades) => {
     if (err) {
      res.json(err);
     }
     res.render('cidades', {
        data: cidades
     });
    });
  });
};

controller.save = (req, res) => {
  const data = req.body;
  console.log(req.body)
  req.getConnection((err, connection) => {
    const query = connection.query('INSERT INTO cidade set ?', data, (err, cidade) => {
      console.log(cidade)
      res.redirect('/cidades');
    })
  })
};

controller.edit = (req, res) => {
  const { cid_codigo } = req.params;
  req.getConnection((err, conn) => {
    conn.query("SELECT * FROM cidade WHERE cid_codigo = ?", [cid_codigo], (err, rows) => {
      res.render('editar-cidade', {
        data: rows[0]
      })
    });
  });
};

controller.update = (req, res) => {
  const { cid_codigo } = req.params;
  const newManager = req.body;
  req.getConnection((err, conn) => {

  conn.query('UPDATE cidade set ? where cid_codigo = ?', [newManager, cid_codigo], (err, rows) => {
    res.redirect('/cidades');
  });
  });
};

controller.delete = (req, res) => {
  const { cid_codigo } = req.params;
  req.getConnection((err, connection) => {
    connection.query('DELETE FROM cidade WHERE cid_codigo = ?', [cid_codigo], (err, rows) => {
      res.redirect('/cidades');
    });
  });
}

module.exports = controller;
