const controller = {};

controller.list = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query("SELECT * FROM view_rodovias", (err, rodovias) => {
      console.log(rodovias)
      if (err) {
        res.json(err);
      }
      res.render("rodovias", {
        data: rodovias
      });
    });
  });
};

controller.save = (req, res) => {
  const data = req.body;
  console.log(req.body);
  req.getConnection((err, connection) => {
    const query = connection.query(
      "INSERT INTO rodovia set ?",
      data,
      (err, rodovia) => {
        console.log(rodovia);
        res.redirect("/rodovias");
      }
    );
  });
};

controller.edit = (req, res) => {
  const { rod_codigo } = req.params;
  req.getConnection((err, conn) => {
    conn.query(
      "SELECT * FROM rodovia WHERE rod_codigo = ?",
      [rod_codigo],
      (err, rows) => {
        res.render("editar-rodovia", {
          data: rows[0]
        });
      }
    );
  });
};

controller.update = (req, res) => {
  const { rod_codigo } = req.params;
  const newManager = req.body;
  req.getConnection((err, conn) => {
    conn.query(
      "UPDATE rodovia set ? where rod_codigo = ?",
      [newManager, rod_codigo],
      (err, rows) => {
        res.redirect("/rodovias");
      }
    );
  });
};

controller.delete = (req, res) => {
  const { rod_codigo } = req.params;
  req.getConnection((err, connection) => {
    connection.query(
      "DELETE FROM rodovia WHERE rod_codigo = ?",
      [rod_codigo],
      (err, rows) => {
        res.redirect("/rodovias");
      }
    );
  });
};

module.exports = controller;
